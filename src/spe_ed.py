import asyncio
import json
import random
import sys

import nest_asyncio
import websockets

from spe_ed_game.src.game.directions import Direction
from spe_ed_game.src.game.moves import Move
from spe_ed_game.src.game.player import Player
from spe_ed_game.src.game.spe_ed_game import Game


class GameServer:
    FPS = 100  # Frames per second.

    width = 50
    height = 50
    blocksize = 20

    connected = set()

    playing = False

    async def start_game(self, playerset: set):
        # start game with given players and settings
        print("Starting new game with " + str(len(playerset)) + " Players")
        players = []
        for player in playerset:
            players.append(Player((random.randint(0, self.width - 1),
                                   random.randint(0, self.height - 1)),
                                  random.choice(list(Direction)),
                                  [random.randint(0, 255), random.randint(0, 255), random.randint(0, 255)],
                                  player))
        Player.count = 1
        game = Game(self.width, self.height, players, self.FPS, True, self.blocksize)

        # run game
        while game.running():
            for player in players:
                if not player.ws.closed:
                    # send game state to every player
                    await player.ws.send(game.returnJson(player))
            for player in players:
                if player.alive:
                    # wait for response of every player
                    move: Move = Move(json.loads(await player.ws.recv())["action"])
                    game.makeMove(player.num - 1, move)
            game.update()
        # end game and save final board
        game.save_screen("./screenshots")
        game.exit()

    async def handler(self, websocket, path):
        # register new user
        print("New User")
        self.connected.add(websocket)

        # deny new users when allready playing
        if self.playing:
            await websocket.send("Already Playing.\nTry again later")
            self.connected.remove(websocket)
        else:
            # wait for new players
            if len(self.connected) < self.game_player_size:
                while True:
                    await asyncio.sleep(100)
                    if websocket.closed:
                        break
                print("Canceling connection")
            else:
                # start game -> enough players are connected
                self.playing = True
                game = self.connected.copy()
                self.connected.clear()
                # run game until its finished
                asyncio.get_event_loop().run_until_complete(
                    asyncio.wait_for(asyncio.ensure_future(self.start_game(game)), 100000))
                print("Game Ende")
                self.playing = False
                # disconnect all
                for player in game:
                    print("Closing connection")
                    await player.close()

    def main(self):
        # get arguments
        if len(sys.argv) != 3:
            print("Wrong parameters!")
            sys.exit(-1)
        try:
            # player size
            self.game_player_size = int(sys.argv[1])
            #port
            self.port = int(sys.argv[2])
        except:
            print("Wrong parameters!")
            sys.exit(-1)
        loop = asyncio.get_event_loop()

        # start server
        start_server = websockets.serve(self.handler, "localhost", self.port)

        print("Starting Server at localhost:"+str(self.port)+" with " + str(self.game_player_size) + " players")

        # wait for connections
        loop.run_until_complete(start_server)
        try:
            loop.run_forever()
        except KeyboardInterrupt:
            pass


if __name__ == "__main__":
    nest_asyncio.apply()
    gameServer = GameServer()
    gameServer.main()
