<!--
*** Build using the Best-README-Template.
-->

<!-- PROJECT LOGO -->
<br />
<p align="center">
![SPE_ED](spe_ed_logo_side.png "SPE_ED Webserver")
  <h3 align="center">SPE_ED Webserver</h3>

  <p align="center">
    Python implementation of a web server for the game spe_ed<br />
    This is a project developed for the 2021 InformatiCup
    <br />
    <br />
    <a href="https://gitlab.gwdg.de/spe_ed/spe_ed_webserver/issues">Report Bug</a>
    ·
    <a href="https://gitlab.gwdg.de/spe_ed/spe_ed_webserver/issues">Request Feature</a>
  </p>
</p>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li>
      <a href="#built-with">Built With</a>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li>
      <a href="#usage">Usage</a>
    </li>
  </ol>

</details>

### Built With

<div style="display: -ms-flexbox;     display: -webkit-flex;     display: flex;     -webkit-flex-direction: row;     -ms-flex-direction: row;     flex-direction: row;     -webkit-flex-wrap: wrap;     -ms-flex-wrap: wrap;     flex-wrap: wrap;     -webkit-justify-content: space-around;     -ms-flex-pack: distribute;     justify-content: space-around;     -webkit-align-content: stretch;     -ms-flex-line-pack: stretch;     align-content: stretch;     -webkit-align-items: flex-start;     -ms-flex-align: start;     align-items: flex-start;">
<a href="https://www.python.org/"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Python-logo-notext.svg/64px-Python-logo-notext.svg.png" alt="Python" width="64" height="64" title="Python"></a>
</div>



<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites
* [Python 3.9.x](https://www.python.org/downloads/release/python-391/)
* [Python pip](https://pypi.org/project/pip/)

### Installation

1. Clone the repo
  ```sh
  git clone https://gitlab.gwdg.de/spe_ed/spe_ed_webserver.git
  ```
2. Create venv if you dont have the /venv/ folder
```sh
pip install virtualenv && virtualenv venv
```

3. Activate venv 
    - Linux: 
        ```sh
        source venv/Scripts/activate
        ```
    - Windows: 
        ```bat
        venv\Scripts\activate.bat
        ```

4. Install Dependencies
```sh
pip install -r requirements.txt
```

5. Update submodules:
```
git submodule update --init --recursive
cd spe_ed_game
git checkout master
```

<!-- USAGE EXAMPLES -->
## Usage

Either use the config in the .run/ folder or
```sh
python3 -m src.spe_ed <number of players> <port>
```
